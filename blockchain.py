from time import time
from os import path
import hashlib
import json
from Account import Account
from urllib.request import urljoin
import requests
from Miner import Miner


# Основной скелет блокчейна
class BlockChain(object):
    # При создании блокчейна устанавливается массив блоков, транзакций, словарь для балансов аккаунтов и множество для
    # нод
    def __init__(self, exist_node=None):
        self.chain = []
        self.transactions = []
        self.nodes = set()
        self.headers = {
            'content-type': 'application/json'
        }
        self.balances = {}
        self.reward = 3
        self.have_new_block = False
        self.node_url = 'http://192.168.1.4:8080'
        self.account = None
        self.handlers = {}
        self.miner = None

        if not exist_node:
            self.create_genesis_block()
        else:
            if self.connect_node(exist_node):
                self.consensus()
                for node in self.nodes:
                    self.notify_node(node)
            else:
                print('Не удалось подключиться к ноде.')

    # Создается новый перевод: в качестве аргументов получаем отправителя, принимающего,
    # сумму и добавляем в массив переводов новый перевод, вернув его индекс
    def create_new_transaction(self, sender_account, recipient, amount):
        # структура транзакции
        transaction = {
            'sender': sender_account.get_public_key(),
            'signature': "",
            'recipient': recipient,
            'amount': amount,
            'timestamp': time()
        }

        if sender_account.get_public_key() in self.balances and self.balances[sender_account.get_public_key()] >= amount:

            transaction['signature'] = sender_account.sign(self.hash(transaction))

            # добавляем транзакцию в массив и возвращаем её индекс
            transaction_index = self.transactions.append(transaction)

            self.run_handlers('create_transaction', transaction)

            return transaction_index
        return False

    def auth(self, public_key, private_key):
        self.account = Account(public_key, private_key)

    def create_genesis_block(self):
        block = {
            'index': 0,
            'previous_hash': '',
            'transactions': [],
            'miner': '',
            'signature': '',
            'timestamp': time(),
            'proof': 338
        }

        # добавляем блок в блокчейн
        self.chain.append(block)
        return True

    # Создается новый блок: в качестве аргументов получаем адрес майнера, подпись,
    # доказательство, предыдущий хэш и добавляем в блокчейн новый блок,
    # очистив массив для переводов вернув индекс нового блока
    def create_new_block(self, miner_account, proof, previous_hash=None):
        # структура блока
        block = {
            'index': len(self.chain),
            'previous_hash': previous_hash,
            'transactions': self.transactions,
            'miner': miner_account.get_public_key(),
            'signature': '',
            'timestamp': time(),
            'proof': proof
        }

        # подписываем блок
        block['signature'] = miner_account.sign(self.hash(block))

        # добавляем блок в блокчейн
        index = self.chain.append(block)
        
        # перерасчитываем балансы пользователей
        self.balances = {}
        self.parse_balances()

        # очищаем массив переводов и возвращаем индекс блока
        self.transactions = []

        self.run_handlers('create_block', block)

        return index

    # функция для получения хэша (блок или перевод преобразуется в строку и хэшируется)
    @staticmethod
    def hash(item):
        item = item.copy()
        if 'signature' in item:
            item.pop('signature')
        return hashlib.sha256(json.dumps(item, sort_keys=True).encode('utf8')).hexdigest()

    # # hash function (block - > string and hash it)
    # @staticmethod
    # def hash_block(block):
    #     if 'signature' in block:
    #         block.pop('signature')
    #     return hashlib.sha256(json.dumps(block, sort_keys=True)).hexdigest().decode('utf8')
    #
    # # hash function (transaction - > string and hash it)
    # @staticmethod
    # def hash_transaction(transaction):
    #     if 'signature' in transaction:
    #         transaction.pop('signature')
    #     return hashlib.sha256(json.dumps(transaction, sort_keys=True))

    # функция перевода монет в рубли
    @staticmethod
    def convert_to_rub(amount):
        return amount * 68.32

    # проверка правильности блокчейна, возвращает истину, если все блоки прошли проверку
    def verify_chain(self, chain):
        for i in range(1, len(chain)):
            answer = self.verify_block(chain[i], chain[i - 1])
            if not answer:
                return False
        return True

    # Проверка валидности подтверждения работы
    @staticmethod
    def valid_proof(last_proof, proof):
        return hashlib.sha256(f'{last_proof}{proof}'.encode('utf8')).hexdigest()[:5] == "00000"

    # функция проверяет блок и возвращает истину в случае целостности
    def verify_block(self, block, previous_block):

        # три флага для различных проверок
        previous_hash_verify = False
        proof_correct_flag = False
        signature_correct_flag = False

        prev_proof = previous_block['proof']
        proof = block['proof']

        # если предыдущий хэш = хэшу от предыдущего блока и если
        # предыдущее доказательство * нынешнее доказательство = хэш, в котором последние 4 символа 0 и
        # если подпись верна
        if block['previous_hash'] == self.hash(previous_block):
            previous_hash_verify = True
        if self.valid_proof(prev_proof, proof):
            proof_correct_flag = True
        if Account.verify(self.hash(block), block['signature'], block['miner']):
            signature_correct_flag = True

        # тогда возвращаем истину
        if previous_hash_verify and proof_correct_flag and signature_correct_flag:
            return True
        # иначе возвращаем ложь
        else:
            return False

    # функция проверяет перевод и возвращает истину или ложь
    @staticmethod
    def verify_transaction(transaction):
        if Account.verify(hash(transaction), transaction['signature'], transaction['sender']):
            return True
        else:
            return False

    # функция получения списка всех нод
    def get_nodes_addresses(self):
        nodes = list(self.nodes)
        return nodes

    # функция присоединения к ноде и получения через неё списка адресов всех нод (сид-нода)
    def connect_node(self, exist_node):
        try:
            response = requests.request('GET', urljoin(exist_node, 'nodes'))
            if response.status_code == 200:
                nodes = response.json()
                nodes.append(exist_node)
                self.nodes = set(nodes).difference({self.node_url, })
                return True
        except requests.exceptions.ConnectionError:
            return False
        return False

    def give_node_address(self, nodes):
        pass

    # def add_new_node(self, exist_node):
    #     #     response = requests.request('GET', urljoin(exist_node, 'chain'))
    #     #     pass

    # получение экземпляра всего блокчейна
    @staticmethod
    def get_blockchain(exist_node):
        response = requests.request('GET', urljoin(exist_node, 'chain'))
        if response.status_code == 200:
            chain = response.json()
            return chain
        else:
            return False

    # механизм консенсуса
    def consensus(self):
        maxLength = len(self.chain)
        updated = False
        for node in self.nodes:
            if self.verify_chain(self.get_blockchain(node)):
                if len(self.get_blockchain(node)) > maxLength:
                    maxLength = len(self.get_blockchain(node))
                    self.chain = self.get_blockchain(node)
                    updated = True
        if updated:
            self.parse_balances()
        return updated

    # функция передачи нодам данных о том, что новый блок был создан
    def new_block_was_created(self):
        payload = json.dumps(self.chain[-1])
        for node in self.nodes:
            requests.post(urljoin(node, 'block_add'), data=payload, headers=self.headers)

    # функция перерасчета баланса у аккаунтов при изменении блокчейна
    def parse_balances(self):
        for block in self.chain:
            if block['miner']:

                if block['miner'] not in self.balances:
                    self.balances[block['miner']] = 0

                self.balances[block['miner']] += self.reward
            for transact in block['transactions']:
                if transact['recipient'] not in self.balances:
                    self.balances[transact['recipient']] = 0
                self.balances[transact['sender']] -= transact['amount']
                self.balances[transact['recipient']] += transact['amount']

    def notify_node(self, node_url):
        payload = json.dumps({'node_url': self.node_url})
        requests.post(urljoin(node_url, 'notify'), data=payload, headers=self.headers)

    def on(self, event, handler):
        if event not in self.handlers:
            self.handlers[event] = []

        self.handlers[event].append(handler)

    def run_handlers(self, event, *args):
        if event in self.handlers:
            for handler in self.handlers[event]:
                handler(*args)

    def append_block(self, block):
        if self.verify_block(block, self.chain[-1]):
            self.chain.append(block)
            return True
        else:
            return False

    def mine(self, account, start=True):
        if self.miner:
            self.miner.started = start

            if not self.miner.is_alive() and start:
                self.miner = Miner(self, account)
                self.miner.start()
        else:
            self.miner = Miner(self, account)
            self.miner.start()

    def get_balance(self, address):
        if address in self.balances:
            return self.balances[address]
        else:
            return 0

    def save_chain(self):
        NODE_DIR = path.dirname(__file__)
        with open(path.join(NODE_DIR, 'chain.json'), 'w', encoding='utf8') as f:
            json.dump(self.chain, f)

    # def close_block(self, sign):

    #     self.chain[len(self.chain) - 1]['signature'] = sign

    # def __new__(cls, *args, **kwargs):
    #     if not hasattr(cls, 'instance'):
    #         cls.instance = super().__new__(cls, *args, **kwargs)
    #
    #     return cls.instance
