from flask import Flask, request, jsonify
from flask_socketio import SocketIO
from blockchain import BlockChain
from CLI import CLI
from WebThread import WebThread
from os import path
import json
from flask import Response
from Account import Account

NODE_DIR = path.dirname(__file__)
CONFIG = {}

with open(path.join(NODE_DIR, 'config.json'), 'r', encoding='utf8') as f:
    CONFIG = json.load(f)


blockchain = BlockChain(CONFIG['SEED_NODE'])

app = Flask(__name__)
io = SocketIO(app, cors_allowed_origins='*')

@app.after_request
def add_cors(response):
    response.headers['Access-Control-Allow-Origin'] = '*'
    return response

@app.route('/')
def index():
    return 'OK'


@app.route('/create_wallet')
def create_wallet():
    user = Account.create()
    pub, priv = user.keys()
    return jsonify({'public_key': pub, 'private_key': priv})


@app.route('/nodes')
def nodes():
    return jsonify(list(blockchain.nodes))


@app.route('/transactions')
def transactions():
    return jsonify(blockchain.transactions)


@app.route('/chain')
def chain():
    resp = jsonify(blockchain.chain)
    resp.headers['Access-Control-Allow-Origin'] = '*'
    return resp


@app.route('/notify', methods=['POST'])
def node_notify():
    data = request.json
    if 'node_url' in data:
        blockchain.nodes.add(data['node_url'])
    return jsonify({'accepted': True})


@app.route('/block_add', methods=['POST'])
def block_found():
    new_block = request.json
    accepted = blockchain.append_block(new_block)
    return jsonify({'accepted': accepted})


def send_io_block(block):
    io.emit('new_block', block)


def send_io_tx(tx):
    io.emit('new_transaction', tx)


web_thread = WebThread(io, app, '0.0.0.0', 8080)
web_thread.start()

blockchain.on('create_block', send_io_block)
blockchain.on('create_transaction', send_io_tx)

console_ui = CLI(blockchain, CONFIG['NODE_PUBLIC_KEY'], CONFIG['NODE_PRIVATE_KEY'])
console_ui.run()

web_thread.stop_server()