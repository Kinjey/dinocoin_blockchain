import hashlib
from threading import Thread
from time import time


# скелет майнера
class Miner(Thread):
    # обращаемся к текущему блокчейну и аккаунту, который запустил майнинг
    def __init__(self, blockchain, account):
        super().__init__()
        self.blockchain = blockchain
        self.account = account
        self.started = True
        self.setDaemon(True)

    def run(self):

        # первичное доказательство равно нулю, берем хэш от старого и нового доказательства, до тех пор,
        # пока на конце хэша не будет четыре нуля, после чего возвращаем новое доказательство

        # для упрощения кода
        chain = self.blockchain.chain

        prev_proof = chain[-1]['proof']
        # изначально новое доказательство равно нулю, получаем хэш от старого и нового доказательства
        proof = 0
        # NewHash = hashlib.sha256(f'{proof * NewProof}'.encode('utf8')).hexdigest()

        # пока хэш от старого и нового доказательства не будет содержать четыре нуля на конце,
        # добавляем к доказательству единицу
        while not self.blockchain.valid_proof(prev_proof, proof) and self.started:
            if not self.blockchain.have_new_block:
                proof += 1
                # NewHash = hashlib.sha256(f'{proof * NewProof}'.encode('utf8')).hexdigest()
            else:
                return 0

        # если раньше нас не нашли доказательство и мы сделали это первыми, то создаем новый блок
        # и оповещаем об этом все подключенные ноды
        self.blockchain.create_new_block(self.account, proof, self.blockchain.hash(chain[len(chain) - 1]))
        self.blockchain.new_block_was_created()
