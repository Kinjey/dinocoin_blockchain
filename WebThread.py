from threading import Thread


class WebThread(Thread):
    def __init__(self, io, app, host, port):
        super().__init__()
        self.io = io
        self.app = app
        self.host = host
        self.port = port
        self.setDaemon(True)

    def run(self):
        self.io.run(self.app, host=self.host, port=self.port)

    def stop_server(self):
        self.io.stop()